﻿namespace SourceControlApi.Domain
{
    public class Repository
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Language { get; set; }
        public string StarCount { get; set; }
        public string HomePage { get; set; }
        public string CloneUrl { get; set; }
        public string RepositoryHost { get; set; }
    }
}