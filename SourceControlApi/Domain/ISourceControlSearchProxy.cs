﻿using System.Collections.Generic;

namespace SourceControlApi.Domain
{
    public interface ISourceControlSearchProxy
    {
        IEnumerable<Repository> GetRepositoriesByLanguage(string language, int count);
    }
}