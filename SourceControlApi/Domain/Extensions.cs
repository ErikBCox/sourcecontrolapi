﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SourceControlApi.Domain
{
    internal static class Extensions
    {
        internal static IEnumerable<Repository> ToRepositoryList(this GithubResultItem[] githubResultItems)
        {
            foreach (var githubResultItem in githubResultItems)
            {
                yield return githubResultItem.ToRepository();
            }
        }
        internal static Repository ToRepository(this GithubResultItem githubResultItem)
        {
            var repo = new Repository();
            repo.CloneUrl = githubResultItem.clone_url;
            repo.Description = githubResultItem.description;
            repo.HomePage = githubResultItem.homepage;
            repo.Language = githubResultItem.language;
            repo.StarCount = githubResultItem.stargazers_count;
            repo.HomePage = githubResultItem.homepage;
            repo.Name = githubResultItem.name;
            repo.Id = githubResultItem.id;
            repo.RepositoryHost = "GitHub";
            return repo;
        }
    }
}