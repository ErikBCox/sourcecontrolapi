﻿using System.Collections.Generic;
using System.Web;
using Newtonsoft.Json;

namespace SourceControlApi.Domain
{
    public class GitHubSearchSearchProxy: ISourceControlSearchProxy
    {
        public IEnumerable<Repository> GetRepositoriesByLanguage(string language, int count)
        {
            var gitHubString = GetByLanguage(language,count);
            var result = JsonConvert.DeserializeObject<GithubResult>(gitHubString);
            return result.items.ToRepositoryList();
        }

        private string GetByLanguage(string language, int count)
        {
            return HttpHelper.Get($"https://api.github.com/search/repositories?q=language:{HttpUtility.UrlEncode(language)}&sort=stars&order=desc&per_page={count}");
        }
    }

    internal class GithubResult
    {
        public GithubResultItem[] items { get; set; }
    }

    internal class GithubResultItem
    {
        public int id { get; set; }
        public string name { get; set; }
        public string full_name { get; set; }
        public string description { get; set; }
        public string html_url { get; set; }
        public string language { get; set; }
        public string stargazers_count { get; set; }
        public string homepage { get; set; }
        public string clone_url { get; set; }
    }
}