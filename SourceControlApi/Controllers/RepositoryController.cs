﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using SourceControlApi.Domain;

namespace SourceControlApi.Controllers
{
    public class RepositoryController : ApiController
    {
        public RepositoryController(ISourceControlSearchProxy sourceControlSearchProxy)
        {
            _sourceControlSearchProxy = sourceControlSearchProxy;
        }

        [Route("api/search")]
        [HttpGet]
        public HttpResponseMessage GetRepositoriesByLanguage(string language)
        {
            try
            {
                var response = _sourceControlSearchProxy.GetRepositoriesByLanguage(language, DEFAULT_RESULT_COUNT);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
        }

        private readonly ISourceControlSearchProxy _sourceControlSearchProxy;
        private const int DEFAULT_RESULT_COUNT = 5;
    }
}
