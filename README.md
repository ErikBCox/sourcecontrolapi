# README #

build a web api that calls GitHub api and returns top 5 starred repos for a user-supplied language

### Using this api ###

* Repos in Assembly - http://localhost:63773/api/search?language=assembly
* Repos in C# - http://localhost:63773/api/search?language=C%23
* Repos in Javascript - http://localhost:63773/api/search?language=javascript